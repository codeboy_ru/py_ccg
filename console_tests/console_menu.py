import curses
from curses import panel


class Menu(object):
    def __init__(self, items, stdscreen):
        self.window = curses.newwin(0, 0)
        self.window.keypad(1)
        self.panel = panel.new_panel(self.window)
        self.panel.hide()
        panel.update_panels()

        self.position = 0
        self.items = items
        self.items.append(('exit', 'exit'))

    def navigate(self, n):
        self.position += n
        if self.position < 0:
            self.position = 0
        elif self.position >= len(self.items):
            self.position = len(self.items) - 1

    def display(self):
        self.panel.top()
        self.panel.show()
        self.window.clear()

        while True:
            self.window.refresh()
            curses.doupdate()
            for index, item in enumerate(self.items):
                if index == self.position:
                    mode = curses.A_REVERSE
                else:
                    mode = curses.A_NORMAL

                msg = '%d. %s' % (index, item[0])
                self.window.addstr(1 + index, 1, msg, mode)

            key = self.window.getch()

            if key in [curses.KEY_ENTER, ord('\n')]:
                if self.position == len(self.items) - 1:
                    break
                else:
                    self.items[self.position][1]()

            elif key == curses.KEY_UP:
                self.navigate(-1)

            elif key == curses.KEY_DOWN:
                self.navigate(1)

        self.window.clear()
        self.panel.hide()
        panel.update_panels()
        curses.doupdate()


class MyApp(object):
    def __init__(self, stdscreen):
        self.screen = stdscreen
        try:
            curses.curs_set(0)
        except:
            pass

        submenu_items = [
            ('beep', curses.beep),
            ('flash', curses.flash)
        ]
        submenu = Menu(submenu_items, self.screen)

        main_menu_items = [
            ('beep', self.some_work2),
            ('flash', curses.flash),
            ('submenu', submenu.display)
        ]
        main_menu = Menu(main_menu_items, self.screen)
        main_menu.display()

    def make_panel(self, h, l, y, x, str):
        win = curses.newwin(h, l, y, x)
        win.erase()
        win.box()
        win.addstr(2, 2, str)

        panel = curses.panel.new_panel(win)
        return win, panel

    def some_work(self):
        # print 'aaaaaa'
        self.window = self.screen.subwin(0, 0)
        self.window.clear()
        panel.update_panels()
        curses.doupdate()
        # self.window.keypad(1)
        self.panel = panel.new_panel(self.window)
        self.panel.top()
        self.panel.show()
        self.window.clear()
        self.window.addstr(9, 1, "dsdsdssd")

        # self.panel.hide()
        # panel.update_panels()
        # curses.doupdate()
        # self.screen.addstr(9, 1, "dsdsdssd")
        # self.screen.refresh()

    def some_work2(self):
        print 'aaaaaaaaaa'
        win1, panel1 = self.make_panel(10, 12, 5, 5, "Panel 1")
        panel1.top()
        curses.panel.update_panels()
        self.screen.refresh()


if __name__ == '__main__':
    curses.wrapper(MyApp)