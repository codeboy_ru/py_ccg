import asyncio

import tornado.concurrent
import tornado.ioloop
import tornado.web
import tornado.platform.asyncio
import tornado.httpclient

def coroutine(func):
  func = asyncio.coroutine(func)
  def decorator(*args, **kwargs):
    future = tornado.concurrent.Future()
    def future_done(f):
      try:
        future.set_result(f.result())
      except Exception as e:
        future.set_exception(e)
    asyncio.async(func(*args, **kwargs)).add_done_callback(future_done)
    return future
  return decorator

def future_wrapper(f):
  future = asyncio.Future()
  def handle_future(f):
    try:
      future.set_result(f.result())
    except Exception as e:
      future.set_exception(e)
  tornado.ioloop.IOLoop.current().add_future(f, handle_future)
  return future

class ReqHandler(tornado.web.RequestHandler):
  @coroutine
  def get(self):
    self.write("Hello world!\n")
    print("Hej!")
    yield from asyncio.sleep(2)
    print("Hej igen!")
    res = yield from future_wrapper(tornado.httpclient.AsyncHTTPClient().fetch("http://google.com"))
    print(res)
    self.write("Hello test\n")

app = tornado.web.Application([
  (r'/', ReqHandler)
])

if __name__ == '__main__':
  tornado.platform.asyncio.AsyncIOMainLoop().install()
  app.listen(8080)
  asyncio.get_event_loop().run_forever()