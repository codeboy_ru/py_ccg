# coding: utf-8

import warnings
import json
from pprint import pprint
import curses
from curses import panel
import time

# from django_apps.dj_site.models import Card


class CardWork(object):

    all_cards = 0
    saved_cards = 0
    cards_list_updated = list()

    def __init__(self, screen):
        self.screen = screen
        # self.win_main = self.create_window()

    def create_window(self, lines=0, cols=0, begin_x=0, begin_y=0, box=1, refresh=True):
        win = curses.newwin(lines, cols, begin_x, begin_y)
        # win.erase()
        if box: win.box()
        if refresh: win.refresh()
        return win

    def create_panel(self, win):
        new_panel = curses.panel.new_panel(win)
        panel.update_panels()
        return new_panel


    def draw_menu(self):
        # win = self.screen
        win = self.create_window()
        card_panel = self.create_panel(win)

        win.addstr(0,0, '11111')
        win.addstr(1,0, '22222')

        panel.update_panels()
        win.refresh()