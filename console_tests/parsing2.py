# coding: utf-8

import warnings
import json
from pprint import pprint
import curses
from curses import panel
import time

from card_work import CardWork


if __name__ == "__main__":
    screen = curses.initscr()
    curses.noecho()
    curses.cbreak()
    curses.start_color()
    # curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)


    data = open('upload/AllSets.enUS.json')
    f = json.load(data)
    cards_curse=f['Curse of Naxxramas']
    cards_basic=f['Basic']
    cards_expert=f['Expert']
    data.close()

    cw = CardWork(screen)

    # time.sleep(0.5)

    cw.draw_menu()

    curses.endwin()
