# coding: utf-8

import json
from pprint import pprint
import curses
from curses import panel
import time

# from django_apps.dj_site.models import Card


class CardWork(object):

    all_cards = 0
    saved_cards = 0
    cards_list_updated = list()

    def __init__(self, screen):
        self.screen = screen

    def make_card_list(self, card_name):
        if len(self.cards_list_updated) >= 20:
            self.cards_list_updated.pop(0)
        self.cards_list_updated.append(card_name)


    def create_card(self, card):
        win3, panel3 = self.make_panel(0, 0, 0, 0, "Panel 3")
        panel3.top()
        panel.update_panels()
        win3.erase()
        win3.redrawwin()
        win3.refresh()
        # self.screen.refresh()
        # self.screen.clear()

        #display progress
        win3.addstr(0, 0, "Total progress: [{0}:{1}] % {2:.0f}".format(
            self.saved_cards, self.all_cards,
            100 * float(self.saved_cards)/float(self.all_cards)
        ))
        # out worked card
        win3.addstr(1, 0, "Moving file: {0} - {1}".format(
            card['name'], card['id'])
        , curses.color_pair(1))

        # display list of updated cards
        if len(self.cards_list_updated) >=1:
            # for i in reversed(cards_list_updated):
            for i in self.cards_list_updated:
                win3.addstr(3+self.cards_list_updated.index(i), 2, "Update card: {0}".format(i))


        self.make_card_list('%s - %s' %(card['name'], card['id']))

        # self.screen.clear()
        panel.update_panels()
        win3.refresh()


    def find_files(self):
        pass

    def make_panel(self, h, l, y, x, str):
        """  create window and panel  """
        win = curses.newwin(h, l, y, x)
        win.erase()
        # win.box()
        win.addstr(2, 2, str)

        panel = curses.panel.new_panel(win)
        return win, panel



if __name__ == "__main__":
    screen = curses.initscr()
    curses.noecho()
    curses.cbreak()
    curses.start_color()
    # curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)


    data = open('upload/AllSets.enUS.json')
    f = json.load(data)
    cards_curse=f['Curse of Naxxramas']
    cards_basic=f['Basic']
    cards_expert=f['Expert']
    data.close()

    cw = CardWork(screen)

    win1, panel1 = cw.make_panel(10, 12, 0, 0, "Panel 1")
    panel1.top()
    panel.update_panels()
    # screen.refresh()
    time.sleep(0.3)

    panel.update_panels()
    screen.refresh()

    win2, panel2 = cw.make_panel(15, 40, 0, 0, "Panel 2")
    panel.update_panels()
    screen.refresh()

    x = 0
    while x != ord('2'):
        screen.clear()
        panel.update_panels()
        screen.refresh()
        # screen.border(0)
        win2.addstr(1, 2, "I found this cards:")
        win2.addstr(2, 4, "Basic cards - %s" % len(cards_basic))
        win2.addstr(3, 4, "Expert cards - %s" % len(cards_expert))
        win2.addstr(4, 4, "Curse of Naxxramas - %s" % len(cards_curse))
        win2.addstr(5, 2, "")
        win2.addstr(6, 2, "Please enter a number...")
        win2.addstr(7, 4, "1 - Start work")
        win2.addstr(8, 4, "2 - Exit")
        win2.addstr(9, 1, "")
        win2.refresh()

        x = win2.getch()

        if x == ord('1'):
            # screen.refresh()
            # screen.clear()
            try:
                for i in cards_basic[2:25]:
                    cw.all_cards = len(cards_basic)
                    cw.create_card(i)
                    time.sleep(0.1)
                    cw.saved_cards += 1

            finally:
                # curses.echo()
                # curses.nocbreak()
                # curses.endwin()
                pass

            # screen.clear()
            # screen.refresh()
            curses.endwin()

    curses.endwin()
