
BaseApp = function () {
    //using TweenLite.set() takes care of all vendor-prefixes
    TweenLite.set(".cardWrapper", {perspective: 800});
    TweenLite.set(".card", {transformStyle: "preserve-3d"});
    TweenLite.set(".back", {rotationY: -180});
    TweenLite.set([".back", ".front"], {backfaceVisibility: "hidden"});


    this.params = {
//        'base': 'on'
    };

    this.init = function (params) {
        $.extend({}, this.params, params);
        var a = this;

//        a.$content = $('#content');

        // меню
//        a.$menuList = $('#sidebar');
//        a.$menuListItems = $('#sidebar a');
//        a.$urlList = urlList;
//        a.$currentUrl = $(location).attr('hash');

        a.$cardWrapper = $(".cardWrapper");

        a.starterLoadContent();
        a.bindEvents();

    };

    /**
     *   EVENTS & ACTIVATORS
     *******************************************************/
    this.bindEvents = function () {
//        this.$menuListItems.on('click', baseApp.menuClick);
//        $(window).on('click', '.loader', baseApp.contentLoader);

        baseApp.$cardWrapper.on('click', baseApp.cardClick);
        baseApp.$cardWrapper.on({
            mouseenter: baseApp.cardHoverOn,
            mouseleave: baseApp.cardHoverOff
        });
    };


    /********************************************
     *  FUNCTIONS
     *******************************************/

    this.starterLoadContent = function () {
        //a nice little intro;)
//        TweenMax.staggerTo($(".card"), 0.5, {rotationY:-180, repeat:1, yoyo:true}, 0.1);
    };


    /**
     * card click
     *************************************/
    this.cardClick = function () {
        var card = $(this).find(".card")
        var cdata = card.data('turn')

        if (cdata == '1') {
            TweenLite.to($(this).find(".card"), 1.2, {rotationY: 0, ease: Back.easeOut});
            card.data('turn', 0);
        } else {
            TweenLite.to(card, 1.2, {rotationY: 180, ease: Back.easeOut});
            card.data('turn', 1);
        }
    };


    /**
     * cards hover
     */
    this.cardHoverOn = function () {
        console.log('dddd');
        var card = $(this).find(".cardFace");
        card.addClass('borderOn')
    };
    this.cardHoverOff = function () {
        var card = $(this).find(".cardFace");
        card.removeClass('borderOn')
    };

};


$(document).ready(function () {

    baseApp = new BaseApp();
    baseApp.init();

});