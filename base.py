# coding: utf-8

from tornado.options import options, define, parse_command_line
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi

import os
import asyncio
import aiomcache
import peewee_async

import tornado_settings.t_settings as TS

from routes import ROUTES
# from tornadobabel.locale import load_gettext_translations


# define('port', type=int, default=8000)
define('port', type=int, default=TS.PORT)


# todo: add port to class
# todo: remove host from db connect
class Application(tornado.web.Application):
    def __init__(self):
        parse_command_line()
        tornado.options.define('debug', default=True)

        # Prepare IOLoop class to run instances on asyncio
        tornado.ioloop.IOLoop.configure('tornado.platform.asyncio.AsyncIOMainLoop')

        handlers = ROUTES
        handlers += [
            (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": TS.STATIC_PATH}),
        ]
        settings = {
            'template_path' : TS.TEMPLATE_PATH,
            'debug' : TS.DEBUG,
            'static_path': TS.STATIC_PATH,
            'st': TS.STATIC_PATH,
            'cookie_secret' : TS.COOKIE_SECRET
        }
        super().__init__(handlers, **settings)


    # todo: loop work
    # todo: fix json
    def start_loops(self, loop):
        enable_json = False
        # self.loop = loop

        self.database = peewee_async.PostgresqlDatabase('pyccg',
            user=TS.USER, password=TS.PASSWORD, host=TS.SERVER_IP, port='5432')
        loop.run_until_complete(self.database.connect_async(loop=loop))
        self.mc = aiomcache.Client("127.0.0.1", 11211, loop=loop)


if __name__ == "__main__":
    # print("Run Tornado ... http://{0}:{1}".format(TS.SERVER_IP, TS.PORT))
    application = Application()
    application.listen(TS.PORT)

    loop = asyncio.get_event_loop()
    application.start_loops(loop)
    loop.run_forever()