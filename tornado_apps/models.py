# coding: utf-8

import asyncio
from datetime import datetime
import peewee as p
from playhouse.postgres_ext import JSONField
import peewee_async


# database = peewee_async.PostgresqlDatabase('pyccg',
#             user='pyccgadmin', password='321', host='192.168.56.101', port='5432')
database = p.Proxy()


class BaseModel(p.Model):
    # def __init__(self, db):
    #     database.initialize(db)
    #     super(BaseModel, self).__init__()


    class Meta:
        database = database



class Card(BaseModel):
    """
     "name" : "Leeroy Jenkins",
             "cost" : 5,
             "type" : "Minion", Spell, Weapon, Hero, Hero Power, Enchantment
           "rarity" : "Legendary", Free, Common, Rare, Epic, Legendary
          "faction" : "Alliance", Alliance, Horde, Neutral
             "text" : "<b>Charge</b>. <b>Battlecry:</b> Summon two 1/1 Whelps for your opponent.",
        "mechanics" : ["Battlecry", "Charge"],
           "flavor" : "At least he has Angry Chicken.",
           "artist" : "Gabe from Penny Arcade",
           "attack" : 6,
           "health" : 2,
      "collectible" : true,
               "id" : "EX1_116",
            "elite" : true
            race : (None) Beast Demon Dragon Mech Murloc Pirate Totem
            playerClass : Druid Hunter Mage Paladin Priest Rogue Shaman Warlock Warrior


    """
    CT_MINION = 'Minion'
    CT_SPELL = 'Spell'
    CT_WEAPON = 'Weapon'
    CT_HERO = 'Hero'
    CT_ENCH = 'Enchantment'
    CT_HP = 'Hero Power'
    CARD_TYPES = ((1, CT_MINION), (2,CT_SPELL), (3,CT_WEAPON), (4,CT_HERO), (5,CT_ENCH), (6,CT_HP))
    CARD_RARITY = ((1,'None'), (2,'Free'),(3,'Common'),(4,'Rare'),(5,'Epic'),(6,'Legendary'),)
    CARD_FACTION = ((1,'None'), (2,'Neutral'), (3,'Horde'), (4,'Alliance'),)
    CARD_RACE = ((1,'None'), (2,'Beast'), (3,'Demon'), (4,'Dragon'),
                 (5,'Mech'), (6,'Murloc'), (7,'Pirate'), (8,'Totem'),)
    CARD_PCLASS = ((1,'None'), (2,'Druid'), (3,'Hunter'), (4,'Paladin'), (5,'Priest'),
                   (6,'Rogue'), (7,'Shaman'), (8,'Warlock'), (9,'Warrior'), (10,'Mage'), (11, 'Dream'))
    CARD_LANGUAGE = ((1,'En'), (2,'Ru'))


    language = p.IntegerField(choices=1, default=1)

    ingame_id = p.CharField(index=True, unique=True)
    name = p.CharField(index=True)
    cost = p.IntegerField(null=True)
    attack = p.IntegerField(null=True)
    health = p.IntegerField(null=True)
    durability = p.IntegerField(null=True)

    player_class = p.IntegerField(default=1, choices=CARD_TYPES)
    c_type = p.IntegerField(default=1, choices=CARD_TYPES)
    rarity = p.IntegerField(default=1, choices=CARD_RARITY)
    faction = p.IntegerField(default=1, choices=CARD_FACTION)
    race = p.IntegerField(default=1, choices=CARD_RACE)

    mechanics_d = p.TextField(null=True)
    # mechanics_d = JSONField()

    text = p.TextField(null=True)
    inPlayText = p.TextField(null=True)
    flavor = p.TextField(null=True)
    artist = p.TextField(null=True)

    collectible = p.BooleanField(default=False)
    elite = p.BooleanField(default=False)

    def get_type_display(self):
        return dict(self.CARD_TYPES)[self.c_type]
    def get_pclass_display(self):
        return dict(self.CARD_PCLASS)[self.player_class]
    def get_rarity_display(self):
        return dict(self.CARD_RARITY)[self.rarity]
    def get_faction_display(self):
        return dict(self.CARD_FACTION)[self.faction]
    def get_race_display(self):
        return dict(self.CARD_RACE)[self.race]


class Deck(BaseModel):
    pass

class Mecanics(BaseModel):
    pass


class User(BaseModel):
    username = p.CharField(index=True, verbose_name='login')
    password = p.CharField()
    email = p.CharField(null=True, index=True)
    join_date = p.DateTimeField(formats='%d-%m-%Y %H:%M:%S', default=datetime.now())

    class Meta:
        order_by = ('username',)


class Session(BaseModel):
    session_id = p.CharField(index=True, primary_key=True)
    # session_data = JSONField()
    session_data = p.TextField()
    expire_datetime = p.DateTimeField(formats='%d-%m-%Y %H:%M:%S', index=True)