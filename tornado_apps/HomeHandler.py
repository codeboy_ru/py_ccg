# coding: utf-8

from tornado_apps.BaseHandler import BaseHandler
from tornado import gen
import asyncio

from tornado_apps.BaseAsyncHandler import AsyncRequestHandler

# @gen.coroutine
# @asyncio.coroutine
# def ff(aaa):
#     print(aaa)
#     return 111111

class HomeHandler(BaseHandler):

    @gen.coroutine
    # @asyncio.coroutine
    def get(self):
        context = dict()
        context = self.context
        context['status'] = "work in progress"
        # zzz = yield from ff('sssssss')
        # zzz = yield ff('sssssss')
        # print(zzz)


        # t = self.render_string('index.html', **context)
        # self.write(t)

        self.render('main.html', **context)



class CardFlipper(BaseHandler):

    def get(self):
        context = dict()
        context = self.context
        self.render('test_cards-flip.html', **context)


from tornado_apps.db_queries import get_users
class HomeAsyncHandler(AsyncRequestHandler):

    @asyncio.coroutine
    def get_async(self):
        database = self.application.database
        # print(database)
        context = dict()
        context = self.context
        context['status'] = "work in progress"
        # zzz = yield from ff('sssssss')
        # zzz = yield ff('sssssss')
        # print(zzz)
        # zzz = yield from get_users(database)
        # print(zzz)
        # for i in zzz:
        #     print(i.password)

        self.render('main.html', **context)





