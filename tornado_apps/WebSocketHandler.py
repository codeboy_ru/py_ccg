# coding: utf-8

import json
import datetime

from tornado import websocket


clients = dict()


class WebSocketBase(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self,  *args, **kwargs):
        # print args
        # print kwargs

        open_time = datetime.datetime.now()
        open_time_formated = date_format(open_time, 'h:i:s d-m-Y')

        # user_id = self.get_argument("user_id")
        self.user_id = self.get_argument("user_id")
        self.stream.set_nodelay(True)
        clients[self.user_id] = {
            "user_id": self.user_id,
            "stream": self,
            'open_time': open_time,
            'open_time_formated': open_time_formated
        }

    def on_close(self):
        if self.user_id in clients:
            del clients[self.user_id]

    def on_message(self, message):
        # print message

        msg_dec = json.loads(u'%s' % message)
        msg_target = msg_dec['msg_target']
        msg_text = msg_dec['msg_text']

        model_kwargs = {
            'message_complete': message,
            'message_text': msg_text,
            'user_target_id': msg_target,
            'user_sender_id': self.user_id
        }
        # q_message = Messages(**model_kwargs)
        # q_message.save()

        str_target = u'%s' % msg_target
        if str_target in clients:
            stream = clients[str_target]['stream']
            response_data = {
                'msg_text': msg_text,
                'msg_sender': self.user_id,
                # 'msg_time': u'%s' % date_format(q_message.datetime_received, 'h:i:s d-m-Y')
            }
            stream.write_message(json.dumps(response_data))
            # print 'send msg in channel'

        # print clients



    def on_message2(self, message):
        # print "Client %s received a message : %s" % (self.user_id, message)
        # print message
        # print type(message)

        # msg_dec = json.loads(u'%s' % message.decode('utf8'))
        msg_dec = json.loads(u'%s' % message)
        # print msg_dec
        msg_body = msg_dec['msg_body']


        # print msg_dec['msg_body']
        # print msg_dec['msg_body'].encode('utf-8')
        # print msg_dec['msg_body'].decode('utf-8')
        # msg_body = msg_dec['msg_body'].encode('ascii')
        # msg_body = msg_dec['msg_body'].encode('utf-8')

        # msg_body = msg_dec['msg_body'].decode('unicode_escape')
        # msg_body = encoding.force_bytes(msg_body)
        # msg_body = encoding.smart_bytes(msg_dec['msg_body'])
        # msg_body = encoding.force_bytes(msg_dec['msg_body'])

        # msg_body = unicode(encoding.force_bytes(msg_dec['msg_body']), 'utf-8')
        # msg_body = unicode(encoding.force_bytes(msg_dec['msg_body']), 'unicode_escape')

        # print type(msg_body)
        # print charade.detect(msg_body)
        # print msg_body

        # text = encoding.force_bytes(message)


        msg_time = datetime.datetime.now()
        msg_time_formated = date_format(msg_time, 'h:i:s d-m-Y')

        # self.write_message("Client %s received a message : %s" % (self.user_id, message))

        msg_data = {
            'msg_type': 'test',
            # 'msg_time': msg_time,
            'msg_time_formated': msg_time_formated,
            'msg_body': msg_body
        }
        self.write_message(json.dumps(msg_data))

        # q_messages = Messages(
        #     datetime_received = msg_time,
        #     message_complete = message
        # )
        # q_messages.save()



ddd = '''
'add_header', 'allow_draft76', 'application', 'async_callback', 'check_etag_header',
'check_xsrf_cookie', 'clear', 'clear_all_cookies', 'clear_cookie', 'clear_header',
'close', 'compute_etag', 'cookies', 'create_signed_value', 'create_template_loader',
'current_user', 'decode_argument', 'delete', 'finish', 'flush', 'get', 'get_argument',
'get_arguments', 'get_body_argument', 'get_body_arguments', 'get_browser_locale',
'get_cookie', 'get_current_user', 'get_login_url', 'get_query_argument',
'get_query_arguments', 'get_secure_cookie', 'get_status', 'get_template_namespace',
'get_template_path', 'get_user_locale', 'get_websocket_scheme', 'head', 'initialize',
'locale', 'log_exception', 'on_close', 'on_connection_close', 'on_finish', 'on_message',
'on_pong', 'open', 'open_args', 'open_kwargs', 'options', 'patch', 'path_args',
'path_kwargs', 'ping', 'post', 'prepare', 'put', 'redirect', 'render', 'render_string',
'request', 'require_setting', 'reverse_url', 'select_subprotocol', 'send_error', 'set_cookie',
'set_default_headers', 'set_etag_header', 'set_header', 'set_nodelay', 'set_secure_cookie',
'set_status', 'settings', 'static_url', 'stream', 'ui', 'write', 'write_error',
'write_message', 'ws_connection', 'xsrf_form_html', 'xsrf_token']
'''

