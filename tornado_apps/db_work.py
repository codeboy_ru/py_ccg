# coding: utf-8

import asyncio
from datetime import datetime
import peewee as p
from playhouse.postgres_ext import JSONField
import peewee_async

from random import randint
import time

from models import Card, Deck, User, Session, Mecanics


database = peewee_async.PooledPostgresqlDatabase('pyccg', user='pyccgadmin', password='321', host='192.168.56.101', port='5432')
loop = asyncio.get_event_loop()

@asyncio.coroutine
def my_handler():
    obj1 = Card.create(text="Yo, I can do it sync!")
    obj2 = yield from peewee_async.create_object(Card, text="Not bad. Watch this, I'm async!")

    all_objects = yield from peewee_async.execute(Card.select())
    for obj in all_objects:
        print(obj.text)

    obj1.delete_instance()
    yield from peewee_async.delete_object(obj2)
def test_loop():
    loop.run_until_complete(database.connect_async(loop=loop))
    loop.run_until_complete(my_handler())

@asyncio.coroutine
def some_sleep():
    time.sleep(1)
    print('wake')

@asyncio.coroutine
def some_print():
    for i in range(1,5):
        print(i)

@asyncio.coroutine
def drop_tables():
    database.connect()
    try:
        database.drop_tables([Card, Session, User])
    except:
        pass

@asyncio.coroutine
def create_tables():
    database.connect()
    # try:
    yield from database.create_tables([Card, Session, User])
    # except:
    #     pass

@asyncio.coroutine
def create_tables():
    database.connect()
    # yield from database.drop_tables([Card, Session, User])
    # yield from drop_tables()
    # yield from some_sleep()
    # yield from some_print()
    # database.create_tables([Card, Session, User])
    database.create_tables([Card])
    # yield from database.create_tables([Card, Session, User])
    # yield from create_tables()
    print('zzz')
    return True

@asyncio.coroutine
def create_user():
    for i in range(1,100):
        zn = randint(9000,10000)
        zp = randint(9000,10000)
        obj1 = User.create(username = zn, password=zp)


def async_test(f):
    def wrapper(*args, **kwargs):
        coro = asyncio.coroutine(f)
        future = coro(*args, **kwargs)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(future)
    return wrapper


from tornado import gen

@asyncio.coroutine
# @gen.coroutine
def get_users():
    print('!!!!!!!!!!!!!!!!')
    all_objects = yield from peewee_async.execute(User.select().where(User.username==999))
    print(all_objects)
    return all_objects


# create_tables()
# loop = asyncio.get_event_loop()
loop.run_until_complete(database.connect_async(loop=loop))
loop.run_until_complete(create_tables())

# loop.run_until_complete(create_user())
# loop.run_until_complete(get_users())
