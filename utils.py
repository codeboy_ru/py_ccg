# coding: utf-8

from datetime import datetime, date
import re


def cleanup_util(r_str):
    """
    clear string from any non Latin + Cyr + 0-9
    needed for search
    """
    return re.sub(r'[^а-яa-z0-9]', '', r_str.lower(), re.UNICODE)


def lang_stub(text=''):
    return text


def create_django_list(dict2iterate):
    """
    create list FOR choices from dict
    """
    new_list = list()
    for k, v in dict2iterate.iteritems():
        new_list.append([k, v])
    return new_list


def create_dict_from_choices(original_list, reverted=False):
    """
    create dict FROM Django choice list
    """
    created_dict = dict()
    for i in original_list:
        if len(i) > 2:
            return False
        else:
            if reverted:
                created_dict[i[1]] = i[0]
            else:
                created_dict[i[0]] = i[1]
    return created_dict
