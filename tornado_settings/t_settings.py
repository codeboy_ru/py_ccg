# -*- coding: utf-8 -*-

import os

PORT = 8000
TEMPLATE_PATH = 'templates'

# DEBUG = False
DEBUG = True

STATIC_PATH = os.path.join(os.path.dirname(__file__), '', '../static').replace('\\','/')
COOKIE_SECRET = '7f2134a5c7fb34036bf966b755f3cb63ab62d2e990374c69ca0260871da8ae24'

from .t_settings_local import *